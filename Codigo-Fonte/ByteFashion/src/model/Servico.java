package model;

/**
 *
 * @author pedroh
 */
public class Servico {
    private int id;
    private String nome;
    private float valor;
    private String descricao;
    private String codigoServico;
    
    public Servico() {
    }

    public Servico(String nome, float valor, String descricao, String codigoServico) {
        this.nome = nome;
        this.valor = valor;
        this.descricao = descricao;
        this.codigoServico = codigoServico;
    }
    
    public Servico(int id, String nome, float valor, String descricao, String codigoServico) {
        this.id = id;
        this.nome = nome;
        this.valor = valor;
        this.descricao = descricao;
        this.codigoServico = codigoServico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigoServico() {
        return codigoServico;
    }

    public void setCodigoServico(String codigoServico) {
        this.codigoServico = codigoServico;
    }
    
    
    
}
