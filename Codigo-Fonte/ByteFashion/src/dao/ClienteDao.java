/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Cliente;
import model.Endereco;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Giuliano
 */
public class ClienteDao {

    private Connection conn;

    public ClienteDao() throws Exception {
        try {
            this.conn = Conexao.getConexao();
        } catch (Exception e) {
            throw new Exception("Erro: \n" + e.getMessage());
        }
    }

    public ClienteDao(Connection c) {
        this.conn = c;
    }

    public void salvar(Cliente cliente) throws Exception {
        PreparedStatement ps = null;

        if (cliente == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
                      
            String SQL = "INSERT INTO Cliente (cpf, nome, sexo, dataNascimento, telefone) values (?, ?, ?, ?, ?);";

            ps = conn.prepareStatement(SQL);
            ps.setString(1, cliente.getCpf());
            ps.setString(2, cliente.getNome());
            ps.setString(3, cliente.getSexo());
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(4, f.format(cliente.getDataNascimento()));
            ps.setString(5, cliente.getTelefone());
            ps.executeUpdate();
            
            //cliente.setId(getCodigoCliente());//setando o codigo do cliente da tabela para o cliente passado
            
            EnderecoDao endDao = new EnderecoDao(conn);
            endDao.salvar(cliente);

        } catch (SQLException sqle) {
            throw new Exception("Erro ao inserir dados " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public void atualizar(Cliente cliente) throws Exception {
        PreparedStatement ps = null;

        if (cliente == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Cliente SET cpf=?, "
                    + " nome=?, sexo=?, dataNascimento=?, telefone=? "
                    + "where idCliente = ?";

            ps = conn.prepareStatement(SQL);
            ps.setString(1, cliente.getCpf());
            ps.setString(2, cliente.getNome());
            ps.setString(3, cliente.getSexo());
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(4, f.format(cliente.getDataNascimento()));
            ps.setString(5, cliente.getTelefone());
            ps.setInt(6, cliente.getId());

            EnderecoDao endDao = new EnderecoDao(conn);
            endDao.atualizar(cliente);

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public List todosClientes() throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Cliente");
            rs = ps.executeQuery();

            List<Cliente> list = new ArrayList<Cliente>();

            while (rs.next()) {
                int cliente_id = rs.getInt(1);
                String cpf = rs.getString(2);
                String nome = rs.getString(3);
                String sexo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);
                String telefone = rs.getString(6);
                EnderecoDao endDao = new EnderecoDao(conn);
                Endereco end = endDao.procurarEndereco(rs.getInt(1));

                list.add(new Cliente(cpf, nome, sexo, dataNascimento, telefone, end));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public List buscarClienteNome(String nomeBusca) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Cliente where nome like ?");
            ps.setString(1, "%"+nomeBusca+"%");
            rs = ps.executeQuery();

            List<Cliente> list = new ArrayList<Cliente>();

            while (rs.next()) {
                int cliente_id = rs.getInt(1);
                String cpf = rs.getString(2);
                String nome = rs.getString(3);
                String sexo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);
                String telefone = rs.getString(6);
                EnderecoDao endDao = new EnderecoDao(conn);
                Endereco end = endDao.procurarEndereco(rs.getInt(1));

                list.add(new Cliente(cpf, nome, sexo, dataNascimento, telefone, end));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Cliente procurarClienteCpf(String cpf) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Cliente where cpf = ?");
            ps.setString(1, cpf);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o CPF: " + cpf);
            }

            int cliente_id = rs.getInt(1);
            String nome = rs.getString(3);
            String sexo = rs.getString(4);
            Date dataNascimento = rs.getDate(5);
            String telefone = rs.getString(6);
            
            EnderecoDao endDao = new EnderecoDao(conn);
            Endereco end = endDao.procurarEndereco(cliente_id);
            return new Cliente(cliente_id,cpf, nome, sexo, dataNascimento, telefone, end);
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
        
    }
    

    public Cliente procurarClienteId(int id) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Cliente where idCliente = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o id: " + id);
            }

            //int cliente_id = rs.getInt(1);
            String cpf = rs.getString(2);
            String nome = rs.getString(3);
            String sexo = rs.getString(4);
            Date dataNascimento = rs.getDate(5);
            String telefone = rs.getString(6);
            
            EnderecoDao endDao = new EnderecoDao(conn);
            Endereco end = endDao.procurarEndereco(id);
            return new Cliente(id,cpf, nome, sexo, dataNascimento, telefone, end);
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
        
    }

    public void excluir(Cliente cliente) throws Exception {
        PreparedStatement ps = null;

        if (cliente == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            EnderecoDao endDao = new EnderecoDao(conn);
            endDao.excluir(cliente);
            
            ps = conn.prepareStatement("delete from Cliente where idCliente = ?");
            ps.setInt(1, cliente.getId());
            ps.executeUpdate();
            
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados:" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
        
        
    }

    private int getCodigoCliente() throws Exception {
        int codigo = 0;
        Statement st = null;
        ResultSet rs = null;
        try {

            st = conn.createStatement();
            rs = st.executeQuery("select Max(idCliente) from Cliente");
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro que tenha codigo");
            } else {
                codigo = rs.getInt(1);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao procurar codigo:" + sqle);
        } finally {
            st.close();
        }

        return codigo;
    }

}
