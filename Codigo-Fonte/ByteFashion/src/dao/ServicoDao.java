package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Servico;

/**
 *
 * @author pedroh
 */
public class ServicoDao {
    private Connection conn;

    public ServicoDao() throws Exception {
        try {
            this.conn = Conexao.getConexao();
        } catch (Exception e) {
            throw new Exception("Erro: \n" + e.getMessage());
        }
    }

    public ServicoDao(Connection conn) {
        this.conn = conn;
    }

    public void salvar(Servico servico) throws Exception {
        PreparedStatement ps = null;

        if (servico == null) {
            throw new Exception("O serviço recebido não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO Servico (nome, valor, descricao, codigoServico) values (?,?,?,?)";
            ps = conn.prepareStatement(SQL);
            ps.setString(1, servico.getNome());
            ps.setFloat(2, servico.getValor());
            ps.setString(3, servico.getDescricao());
            ps.setString(4, servico.getCodigoServico());

            ps.executeUpdate();

        } catch (SQLException sqle) {
            throw new Exception("Erro ao inserir dados " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }

    }

    public void atualizar(Servico servico) throws Exception {
        PreparedStatement ps = null;

        if (servico == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Servico SET nome=?, "
                    + " valor=?, descricao=?, codigoServico=?"
                    + "where idServico=?";

            ps = conn.prepareStatement(SQL);
            ps.setString(1, servico.getNome());
            ps.setFloat(2, servico.getValor());
            ps.setString(3, servico.getDescricao());
            ps.setString(4, servico.getCodigoServico());
            ps.setInt(5, servico.getId());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public List<Servico> todosServicos() throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Servico");
            rs = ps.executeQuery();

            List<Servico> list = new ArrayList<>();

            while (rs.next()) {
                int idServico = rs.getInt(1);
                String nome = rs.getString(2);
                Float valor = rs.getFloat(3);
                String descricao = rs.getString(4);
                String codigoServico = rs.getString(5);
                
                list.add(new Servico(idServico, nome, valor, descricao,codigoServico));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public ArrayList<Servico> buscarServicosAgendamento(int idAgendamento) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select s.idServico,s.nome,s.valor,s.descricao,s.codigoServico from ServicoAgendamento inner join Servico as s ON ServicoAgendamento.idServico = s.idServico where idAgenda = ?");
            ps.setInt(1, idAgendamento);
            rs = ps.executeQuery();

            ArrayList<Servico> list = new ArrayList<>();
            
            while (rs.next()) {
                int idServico = rs.getInt(1);
                String nome = rs.getString(2);
                Float valor = rs.getFloat(3);
                String descricao = rs.getString(4);
                String codigoServico = rs.getString(5);
                
                list.add(new Servico(idServico, nome, valor, descricao,codigoServico));
            }
            
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public List buscarServicoNome(String nomeBusca) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Servico where nome like ?");
            ps.setString(1, "%"+nomeBusca+"%");
            rs = ps.executeQuery();

            List<Servico> list = new ArrayList<>();

            while (rs.next()) {
                int idServico = rs.getInt(1);
                String nome = rs.getString(2);
                Float valor = rs.getFloat(3);
                String descricao = rs.getString(4);
                String codigoServico = rs.getString(5);
                
                list.add(new Servico(idServico, nome, valor, descricao, codigoServico));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Servico procurarServicoId(int id) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Servico where idServico = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o idAgendamento: " + id);
            }

            int idServico = rs.getInt(1);
            String nome = rs.getString(2);
            Float valor = rs.getFloat(3);
            String descricao = rs.getString(4);
            String codigoServico = rs.getString(5);
            return new Servico(idServico, nome, valor, descricao, codigoServico);

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Servico procurarServico(String nomeBusca) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Servico where nome = ?");
            ps.setString(1, nomeBusca);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o nome: " + nomeBusca);
            }

            int idServico = rs.getInt(1);
            String nome = rs.getString(2);
            Float valor = rs.getFloat(3);
            String descricao = rs.getString(4);
            String codigoServico = rs.getString(5);
            return new Servico(idServico, nome, valor, descricao, codigoServico);

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    public void excluir(Servico Servico) throws Exception {
        PreparedStatement ps = null;

        if (Servico == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            ps = conn.prepareStatement("delete from Servico where idServico = ?");
            ps.setInt(1, Servico.getId());
            ps.executeUpdate();

        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados:" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }
}
