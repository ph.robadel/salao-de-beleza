package model;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alcebiades, pedroh
 */
public class Cliente {
    
    private int id;
    private String cpf;
    private String nome;
    private String sexo;
    private Date dataNascimento;
    private String telefone;
    private Endereco endereco;

    public Cliente(String cpf, String nome, String sexo, Date dataNascimento, String telefone, Endereco endereco) {
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
        this.endereco = endereco;
    }
    public Cliente(int id,String cpf, String nome, String sexo, Date dataNascimento, String telefone, Endereco endereco) {
        this.id = id;
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Cliente\n" + "id=" + id + "\n cpf=" + cpf + "\n nome=" + nome + "\n sexo=" + sexo + "\n dataNascimento=" + dataNascimento + "\n telefone=" + telefone + "\n endereco=" + endereco + '}';
    }
    
    

    public Cliente() {
    }
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
        
}
