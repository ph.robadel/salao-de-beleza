-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bdsalao
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bdsalao
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bdsalao` DEFAULT CHARACTER SET utf8 ;
USE `bdsalao` ;

-- -----------------------------------------------------
-- Table `bdsalao`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`Cliente` (
  `idCliente` INT(11) NOT NULL AUTO_INCREMENT,
  `cpf` CHAR(14) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `sexo` CHAR(1) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  `telefone` CHAR(14) NOT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bdsalao`.`Agendamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`Agendamento` (
  `idAgenda` INT(11) NOT NULL AUTO_INCREMENT,
  `idCliente` INT(11) NOT NULL,
  `data` DATE NOT NULL,
  `horario` TIME NOT NULL,
  `realizado` TINYINT(1) NOT NULL,
  `valorTotal` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`idAgenda`),
  INDEX `fk_Agenda_Cliente_idx` (`idCliente` ASC),
  CONSTRAINT `fk_Agenda_Cliente`
    FOREIGN KEY (`idCliente`)
    REFERENCES `bdsalao`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bdsalao`.`Endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`Endereco` (
  `idEndereco` INT(11) NOT NULL AUTO_INCREMENT,
  `logradouro` VARCHAR(45) NOT NULL,
  `complemento` VARCHAR(45) NULL DEFAULT NULL,
  `cidade` VARCHAR(80) NOT NULL,
  `estado` CHAR(2) NOT NULL,
  `cep` CHAR(9) NULL DEFAULT NULL,
  PRIMARY KEY (`idEndereco`),
  CONSTRAINT `fk_Endereco_Cliente`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `bdsalao`.`Cliente` (`idCliente`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bdsalao`.`Funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`Funcionario` (
  `idFuncionario` INT(11) NOT NULL AUTO_INCREMENT,
  `cpf` CHAR(14) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `cargo` VARCHAR(69) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  PRIMARY KEY (`idFuncionario`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bdsalao`.`Servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`Servico` (
  `idServico` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `valor` FLOAT NOT NULL,
  `descricao` VARCHAR(100) NULL DEFAULT NULL,
  `codigoServico` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idServico`),
  UNIQUE INDEX `codigoServico_UNIQUE` (`codigoServico` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bdsalao`.`ServicoAgendamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdsalao`.`ServicoAgendamento` (
  `idAgenda` INT(11) NOT NULL,
  `idServico` INT(11) NOT NULL,
  `idFuncionario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idAgenda`, `idServico`),
  INDEX `fk_Agendamento_has_Servico_Servico1_idx` (`idServico` ASC),
  INDEX `fk_Agendamento_has_Servico_Agendamento1_idx` (`idAgenda` ASC),
  INDEX `fk_ServicoAgendamento_Funcionario1_idx` (`idFuncionario` ASC),
  CONSTRAINT `fk_Agendamento_has_Servico_Agendamento1`
    FOREIGN KEY (`idAgenda`)
    REFERENCES `bdsalao`.`Agendamento` (`idAgenda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Agendamento_has_Servico_Servico1`
    FOREIGN KEY (`idServico`)
    REFERENCES `bdsalao`.`Servico` (`idServico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ServicoAgendamento_Funcionario1`
    FOREIGN KEY (`idFuncionario`)
    REFERENCES `bdsalao`.`Funcionario` (`idFuncionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
