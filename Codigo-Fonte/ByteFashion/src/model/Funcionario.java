/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author alcebiades
 */
public class Funcionario {
    private int id;
    private String cpf;
    private String nome;
    private String cargo;
    private Date dataNascimento;

    public Funcionario(String cpf, String nome, String cargo, Date dataNascimento) {
        this.cpf = cpf;
        this.nome = nome;
        this.cargo = cargo;
        this.dataNascimento = dataNascimento;
    }

    public Funcionario(int id, String cpf, String nome, String cargo, Date dataNascimento) {
        this.id = id;
        this.cpf = cpf;
        this.nome = nome;
        this.cargo = cargo;
        this.dataNascimento = dataNascimento;
    }

    public Funcionario() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

}
