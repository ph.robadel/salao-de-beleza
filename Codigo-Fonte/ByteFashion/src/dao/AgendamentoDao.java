package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Agendamento;
import model.Funcionario;
import model.Servico;

/**
 *
 * @author pedroh
 */
public class AgendamentoDao {

    private Connection conn;

    public AgendamentoDao() throws Exception {
        try {
            this.conn = Conexao.getConexao();
        } catch (Exception e) {
            throw new Exception("Erro: \n" + e.getMessage());
        }
    }

    public AgendamentoDao(Connection conn) {
        this.conn = conn;
    }

    public void agendar(Agendamento agendamento) throws Exception {
        PreparedStatement ps = null;

        if (agendamento == null) {
            throw new Exception("O agendamento recebido não pode ser nulo");
        }
        
        if (agendamento.getCliente() == null) {
            throw new Exception("O agendamento recebido não pode ter o cliente nulo");
        }
        
        if (agendamento.getServisos() == null || agendamento.getServisos().isEmpty()) {
            throw new Exception("O agendamento recebido não pode ter os serviços nulo ou vazios");
        }
        
        try {
            // ENTIDADE AGENDAMENTO
            String SQL = "INSERT INTO Agendamento(idCliente, data, horario, realizado, valorTotal) values (?, ?, ?, ?, ?)";

            ps = conn.prepareStatement(SQL);
            //ps.setInt(1, agendamento.getId()); AUTOIncremento
            ps.setInt(1, agendamento.getCliente().getId());
            
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(2, f.format(agendamento.getData()));
            
            f = new SimpleDateFormat("HH:mm:ss");
            ps.setString(3, f.format(agendamento.getHorario()));
            
            ps.setBoolean(4, false);
            ps.setFloat(5, agendamento.getValorTotal());
            ps.executeUpdate();
            ps.close();
            
            //PEGAR ID AGENDAMENTO
            ps = conn.prepareStatement("select MAX(idAgenda) as maxId from Agendamento");
            ResultSet rs = ps.executeQuery();
            rs.next();
            agendamento.setId(rs.getInt(1));
            ps.execute();
            ps.close();
            
            // ENTIDADE SERVICOAGENDAMENTO
            SQL = "INSERT INTO ServicoAgendamento(idAgenda, idServico) values (?, ?)";
            for(Servico s : agendamento.getServisos()){
                ps = conn.prepareStatement(SQL);
                ps.setInt(1, agendamento.getId());
                ps.setInt(2,s.getId());
                ps.execute();
                ps.close();
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao inserir dados " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }

    }

    public void atualizar(Agendamento agendamento) throws Exception {
        PreparedStatement ps = null;

        if (agendamento == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Agendamento SET idCliente=?, data=?, horario=?, realizado=?, valorTotal=?"
                    + "where idAgendamento=?";

            ps = conn.prepareStatement(SQL);
            ps.setInt(1, agendamento.getCliente().getId());
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(2, f.format(agendamento.getData()));
            ps.setString(3, f.format(agendamento.getHorario()));
            ps.setBoolean(4, agendamento.isRealizado());
            ps.setFloat(5, agendamento.getValorTotal());
            ps.setInt(6, agendamento.getId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }
    
    
    public ArrayList<Agendamento> buscarAgendamentos() throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Agendamento where realizado = false");
            rs = ps.executeQuery();

            ArrayList<Agendamento> list = new ArrayList<>();
            Agendamento agendamento;
            while (rs.next()) {
                agendamento = new Agendamento();
                agendamento.setId(rs.getInt(1));
                agendamento.setCliente((new ClienteDao()).procurarClienteId(rs.getInt(2)));
                agendamento.setServisos((new ServicoDao()).buscarServicosAgendamento(rs.getInt(2)));
                agendamento.setData(rs.getDate(3));
                agendamento.setHorario(rs.getDate(4));
                agendamento.setRealizado(rs.getBoolean(5));
                agendamento.setValorTotal(rs.getFloat(6));

                list.add(agendamento);
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    

    
    
    public ArrayList<Agendamento> buscarAtendimento() throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Agendamento where realizado = true");
            rs = ps.executeQuery();

            ArrayList<Agendamento> list = new ArrayList<>();
            Agendamento agendamento;
            while (rs.next()) {
                agendamento = new Agendamento();
                agendamento.setId(rs.getInt(1));
                agendamento.setCliente((new ClienteDao()).procurarClienteId(rs.getInt(2)));
                agendamento.setServisos((new ServicoDao()).buscarServicosAgendamento(rs.getInt(2)));
                agendamento.setData(rs.getDate(3));
                agendamento.setHorario(rs.getDate(4));
                agendamento.setRealizado(rs.getBoolean(5));
                agendamento.setValorTotal(rs.getFloat(6));

                list.add(agendamento);
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    public Agendamento procurarAgendamento(int idAgendamento) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Agendamento where idAgenda = ?");
            ps.setInt(1, idAgendamento);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o id: " + idAgendamento);
            }
            
            Agendamento agendamento = new Agendamento();
            agendamento.setId(rs.getInt(1));
            agendamento.setCliente((new ClienteDao()).procurarClienteId(rs.getInt(2)));
            agendamento.setServisos((new ServicoDao()).buscarServicosAgendamento(idAgendamento));
            agendamento.setData(rs.getDate(3));
            agendamento.setHorario(rs.getDate(4));
            agendamento.setRealizado(rs.getBoolean(5));
            agendamento.setValorTotal(rs.getFloat(6));
            return agendamento;

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Agendamento procurarAgendamento(Date data, String horario) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select idAgenda from Agendamento where data = ? and horario = ?");
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(1, f.format(data));
            ps.setString(2, horario);
            rs = ps.executeQuery();
            if (!rs.next()) {
                return null;
            }
            
            return this.procurarAgendamento(rs.getInt(1));

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    public void excluir(Agendamento Agendamento) throws Exception {
        PreparedStatement ps = null;

        if (Agendamento == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            ps = conn.prepareStatement("delete from Agendamento where idAgendamento = ?");
            ps.setInt(1, Agendamento.getId());
            ps.executeUpdate();

        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados:" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }
    
    public void registrarAtendimento(int idAgenda) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try{
            String SQL = ("UPDATE Agendamento SET realizado=? where idAgenda = ?");
            ps = conn.prepareStatement(SQL);
            ps.setBoolean(1, true);
            ps.setInt(2, idAgenda);
            ps.executeUpdate();
            ps.close();
        }catch(SQLException sqle){
            throw new Exception("Erro ao registrar atendimento" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
            
    }
    
    public void registrarFuncionarioServico(int idAgenda, int idServico, int idFuncionario) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        if(idAgenda == 0 || idServico == 0 || idFuncionario == 0){
            throw new Exception("O valor passado não pode ser nulo");
        }
        try{
            String SQL = ("update ServicoAgendamento set idFuncionario = ? where idAgenda =? and idServico = ? ");
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, idFuncionario);
            ps.setInt(2, idAgenda);
            ps.setInt(3, idServico);
            ps.executeUpdate();
            
        }catch(SQLException sqle){
            throw new Exception("Erro ao registrar atendimento" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
            
    }
    public Funcionario buscarFuncionarioServico(int idAgenda, int idServico) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        if(idAgenda == 0 || idServico == 0){
            throw new Exception("O valor passado não pode ser nulo");
        }
        try{
            String SQL = ("select idFuncionario from ServicoAgendamento where idAgenda =? and idServico = ? ");
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, idAgenda);
            ps.setInt(2, idServico);
            rs = ps.executeQuery();
            
            if(rs.next()){
                return (new FuncionarioDao()).procurarFuncionario(rs.getInt(1));
            }    
            
            return null;
        }catch(SQLException sqle){
            throw new Exception("Erro ao registrar atendimento" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
            
    }
    
    
    
    public ArrayList<String> buscarHorariosDisponiveis(Date data) throws Exception{
        PreparedStatement ps = null;
        
        ArrayList<String> lista = new ArrayList();
        lista.add("08:00:00");
        lista.add("09:00:00");
        lista.add("10:00:00");
        lista.add("11:00:00");
        lista.add("12:00:00");
        lista.add("13:00:00");
        lista.add("14:00:00");
        lista.add("15:00:00");
        lista.add("16:00:00");
        lista.add("17:00:00");
        lista.add("18:00:00");
        
        // CRIA A LISTA DE HORÁRIOS POSÍVEIS
        // BUSCA O DIA E HORÁRIO, SE A FOR VÁLIDA A CONSULTA, É PORQUE EXITE ESSA COMBINAÇÃO DE DATA E HORÁRIO
        // SE EXISTIR, EU RETIRO POIS JÁ EXISTE AGENDAMENTO.
        ResultSet rs = null;
        try {
            for(int i = lista.size()-1;i>=0;i--){
                ps = conn.prepareStatement("select * from Agendamento where data = ? and horario = ?");
                
                SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
                ps.setString(1,f.format(data));
                ps.setString(2,lista.get(i));

                rs = ps.executeQuery();
                
                if(rs.next())
                    lista.remove(i);

               
            }
            return lista;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
        
    }
    
    public List<Date> todosHorarios(Date data) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps = conn.prepareStatement("select Agendamento.horario from Agendamento where data = ? limit 8;");
            System.out.println(f.format(data));
            ps.setString(1,f.format(data));
            
            rs = ps.executeQuery();

            List<Date> list = new ArrayList<>();
                while (rs.next()) {
                list.add(rs.getTime(1));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    
}
