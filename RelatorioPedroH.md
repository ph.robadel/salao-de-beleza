## Relatório de atividades Pedro Henrique ## 

Este documento apresenta um relatório pessoal do aluno Pedro Henrique Robadel na implementação do projeto 'ByteFashion', como uma atividade avaliativa da disciplina Tópicos Especias em Programação II da Universidade Federal do Espírito Santo.

### Implementação ###

Para a implementação  do projeto 'ByteFashion' a estratégia adotada da dupla [Pedro Henrique](https://gitlab.com/ph.robadel) e [Alcebiades](https://gitlab.com/alcebiadesogamas) foi de realizar a implementação das classes periféricas (controle de Cliente, Funcionário e Serviço) até ir para as classes mais complexas (Agendamento e atendimento). As tarefas foram dividades inicialmente por entidade do banco de dados, onde cada um implementaria a interface, a classe dao e model. Assim cada um trabalharia em cada aspectos do sistema.

O versionamento e a implementação de cada integrante está disponível para consulta [neste repositório](https://gitlab.com/ph.robadel/salao-de-beleza/commits/master) do gitlab.


### Dificuldades enfrentadas ###
As aulas da disciplina Tópicos Especiais em Programação II foram abordadas com um caráter mais prático. Foi explicado os principais componentes disponíveis na paleta do netbens com suas configurações, propriedades e métodos de manipulação. Com isso, a maior parte da implementação foi realizada sem grandes dificuldades.

A dificuldade enfrentada inicialmente foi a concepção dos requisitos do sistema. Foram propostos poucos requisitos na especificação do trabalho, deixando a elaboração de mais requisitos e detalhamento do problema em aberto para a equipe do trabalho. Foi gasto bastante tempo projetando o trabalho e modelos de banco de dados diferentes.

Outra dificuldade enfrentada foi a implementação das classes do pacote 'dao'. Durante o estudo de caso em sala de aula foi fornecido o modelo dao pronto. Durante a adptação dessas classes ao trabalho, foi encontrada diversas mensagens de erros, e exigiu o estudo de algumas das classes utilizadas, como PreparedStatement (Saber quando usar o `execute()`, `executeQuery()` e `executeUpdate()`) , ResultSet (manipular o retorno da consulta e utilizar corretamento o `next()`).

Outra dificuldade enfrentada, não relacionada diretamente a implementação do trabalho e sim pessoal, foi a gerência de tempo neste final de período de 2019/2 para a realização do trabalho. 

### Materiais de pesquisas ###

Para algumas dúvidas rápidas de métodos e configuração do Swing e seus componentes foram utilizados os slides de aulas disponível no [site da disciplina](http://giulianoti.tk/).

Para algumas mensagens de erro específicas geradas na implementação das classes do pacote 'dao', utilizei o [Stack Overflow em Português](https://pt.stackoverflow.com/), [Stack Overflow](https://stackoverflow.com/) e o [GUJ](https://www.guj.com.br/).

Para estudar os materias de manipulação sql utilizei o seguinte material da DevMedia: [Manipulando dados com JDBC em Java](https://www.devmedia.com.br/manipulando-dados-com-jdbc-em-java/27287).