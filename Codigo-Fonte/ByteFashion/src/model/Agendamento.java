/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author alcebiades, pedroh
 */
public class Agendamento {

    private int id;
    private Cliente cliente;
    private ArrayList<Servico> servisos;
    private Date data;
    private Date horario; //pego com simpleDateFormat("hh:mm:ss") e dou um parse
    private boolean realizado; // se foi atendido;
    private float valorTotal;

    public Agendamento(){
        servisos = new ArrayList<>();
    }

    public Agendamento(int idAgendamento, Cliente cliente, ArrayList<Servico> servisos, Date data, Date horario, boolean realizado, float valorTotal) {
        this.id = idAgendamento;
        this.cliente = cliente;
        this.servisos = servisos;
        this.data = data;
        this.horario = horario;
        this.realizado = realizado;
        this.valorTotal = valorTotal;
    }
    
    public void calcularValorTotal(){
        valorTotal = 0;
        
        for(Servico s: servisos)
            valorTotal += s.getValor();
    }
    
    public void removerServico(String codigo){
        for(int i = 0; i < servisos.size(); i++)
            if(servisos.get(i).getCodigoServico().equals(codigo)){
                servisos.remove(servisos.get(i));
                return;
            }
    }
    
    public boolean isContemServico(int idServico){
        for(Servico s: servisos)
            if(s.getId() == idServico)
                return true;
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int idAgendamento) {
        this.id = idAgendamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Servico> getServisos() {
        return servisos;
    }

    public void setServisos(ArrayList<Servico> servisos) {
        this.servisos = servisos;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public boolean isRealizado() {
        return realizado;
    }

    public void setRealizado(boolean realizado) {
        this.realizado = realizado;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

}
