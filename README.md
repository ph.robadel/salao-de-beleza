## Sistema em Java para o gerenciamento de um salão de beleza ##

Este sistema é uma atividade avaliativa da disciplina Tópicos Especiais em Programação II em 2019/2.

### Equipe de desenvolvimento ###
 - Alcebiades 
 - Pedro Henrique Robadel

### Requisitos: ###

**Controle de atendimento de um salão de beleza**
- Controle da agenda de atendimento
- Cadastro de clientes
- Controle de funcionários que atenderão aos clientes
- Controle de serviços (ex.: corte, escova, manicure, pedicure, maquiagem, etc com seus valores)
- Registrar um atendimento para um cliente. O funcionário responsável por consultar e montar a comanda de serviços, que são os serviços do salão desejados pelo cliente. O sistema deverá registrar automaticamente a data e a hora do serviço completo e o valor total pago pelo cliente.

**Classes**: agenda, cliente, funcionário, serviço

