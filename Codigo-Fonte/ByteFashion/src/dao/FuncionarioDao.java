/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Funcionario;

/**
 *
 * @author alcebiades
 */
public class FuncionarioDao {

    private Connection conn;

    public FuncionarioDao() throws Exception {
        try {
            this.conn = Conexao.getConexao();
        } catch (Exception e) {
            throw new Exception("Erro: \n" + e.getMessage());
        }
    }

    public FuncionarioDao(Connection conn) {
        this.conn = conn;
    }

    public void salvar(Funcionario funcionario) throws Exception {
        PreparedStatement ps = null;

        if (funcionario == null) {
            throw new Exception("O funcionario recebido não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO Funcionario (cpf, nome, cargo, dataNascimento) values (?,?,?,?)";
            ps = conn.prepareStatement(SQL);
            ps.setString(1, funcionario.getCpf());
            ps.setString(2, funcionario.getNome());
            ps.setString(3, funcionario.getCargo());
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(4, f.format(funcionario.getDataNascimento()));

            ps.executeUpdate();

        } catch (SQLException sqle) {
            throw new Exception("Erro ao inserir dados " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }

    }

    public void atualizar(Funcionario funcionario) throws Exception {
        PreparedStatement ps = null;

        if (funcionario == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Funcionario SET nome=?, "
                    + " cargo=?, dataNascimento=?"
                    + "where cpf=?";

            ps = conn.prepareStatement(SQL);
            ps.setString(1, funcionario.getNome());
            ps.setString(2, funcionario.getCargo());
            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
            ps.setString(3, f.format(funcionario.getDataNascimento()));
            ps.setString(4, funcionario.getCpf());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public List<Funcionario> todosFuncionarios() throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Funcionario");
            rs = ps.executeQuery();

            List<Funcionario> list = new ArrayList<Funcionario>();

            while (rs.next()) {
                int idFuncionario = rs.getInt(1);
                String cpf = rs.getString(2);
                String nome = rs.getString(3);
                String cargo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);
                
                list.add(new Funcionario(idFuncionario, cpf, nome, cargo, dataNascimento));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public List buscarFuncionariosNome(String nomeBusca) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {

            ps = conn.prepareStatement("select * from Funcionario where nome like ?");
            ps.setString(1, "%"+nomeBusca+"%");
            rs = ps.executeQuery();

            List<Funcionario> list = new ArrayList<>();

            while (rs.next()) {
                int idFuncionario = rs.getInt(1);
                String cpf = rs.getString(2);
                String nome = rs.getString(3);
                String cargo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);
                
                list.add(new Funcionario(idFuncionario, cpf, nome, cargo, dataNascimento));
            }
            return list;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Funcionario buscarFuncionarioNome(String nome) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Funcionario where nome = ?");
            ps.setString(1, nome);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o CPF: " + nome);
            }

            int idFuncionario = rs.getInt(1);
                String cpf = rs.getString(2);
                //String nome = rs.getString(3);
                String cargo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);


            return new Funcionario(idFuncionario,cpf, nome, cargo, dataNascimento);

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    public Funcionario procurarFuncionario(String cpf) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Funcionario where cpf = ?");
            ps.setString(1, cpf);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o CPF: " + cpf);
            }

            int idFuncionario = rs.getInt(1);
                //String cpf = rs.getString(2); (Já é passado por parametro)
                String nome = rs.getString(3);
                String cargo = rs.getString(4);
                Date dataNascimento = rs.getDate(5);


            return new Funcionario(idFuncionario,cpf, nome, cargo, dataNascimento);

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }
    
    public Funcionario procurarFuncionario(int id) throws Exception {
        PreparedStatement ps = null;

        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("select * from Funcionario where idFuncionario = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("Não foi encontrado nenhum registro com o id: " + id);
            }

            //int idFuncionario = rs.getInt(1);
            String cpf = rs.getString(2);
            String nome = rs.getString(3);
            String cargo = rs.getString(4);
            Date dataNascimento = rs.getDate(5);


            return new Funcionario(id,cpf, nome, cargo, dataNascimento);

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
        }
    }

    public void excluir(Funcionario funcionario) throws Exception {
        PreparedStatement ps = null;

        if (funcionario== null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        try {
            ps = conn.prepareStatement("delete from Funcionario where idFuncionario = ?");
            ps.setInt(1, funcionario.getId());
            ps.executeUpdate();

        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados:" + sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }
}
